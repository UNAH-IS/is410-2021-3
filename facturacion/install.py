"""
Este archivo es el encargado de crear las tablas a la que estan asociados los
modelos del proyecto.
"""

from app import db
from models import *


if __name__ == "__main__":
    db.create_all()
