"""
En este archivo se colocan todas las clases que luego persistiran sus datos
por medio de una BD denominada SQLite.

Se han colocado 3 clases especiales:
- Base: contiene un id y una secuencia de funciones muy utiles para realizar el
CRUD de la clase.
- BaseTimestamp: contiene 2 atributos, creado y modificado.
- BaseDefinicion: contiene 2 atributos, codigo y descripcion. Esto para ciertas
tablas que solamente sirven como diccionarios de datos.
"""

import datetime
from sqlalchemy.ext.declarative import declared_attr

from app import db


class Base(db.Model):
    __abstract__ = True

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, value in data.items():
            setattr(self, key, value)
        self.modificado = datetime.datetime.now()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class BaseTimestamp(db.Model):
    __abstract__ = True

    creado = db.Column(db.DateTime, default=db.func.now())
    modificado = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class BaseDefinicion(db.Model):
    __abstract__ = True

    codigo = db.Column(db.String(10))
    descripcion = db.Column(db.String(50))


##############################################################################


class Empresa(Base):
    nombre = db.Column(db.String(100))
    telefono = db.Column(db.String(20))
    direccion = db.Column(db.String(255))


class Usuario(Base, BaseTimestamp):
    nombre = db.Column(db.String(20))
    password = db.Column(db.String(255))


class Estado_Orden(Base, BaseDefinicion):
    pass


class Impuesto(Base, BaseDefinicion):
    valor = db.Column(db.Numeric(scale=13, precision=2))


class Orden(Base, BaseTimestamp):
    codigo = db.Column(db.String(10))
    numero_caja = db.Column(db.Integer(), nullable=False)
    emision = db.Column(db.DateTime)

    orden_detalle = db.relationship(
        "Detalle",
    )


class OrdenDetalle(Base):
    cantidad = db.Column(db.Numeric(scale=13, precision=2))
    descuento = db.Column(db.Numeric(scale=13, precision=2))
    subtotal = db.Column(db.Numeric(scale=13, precision=2))


class OrdenEstado(Base, BaseDefinicion):
    pass


class Producto(Base, BaseTimestamp):
    codigo = db.Column(db.String(20))
    nombre = db.Column(db.String(100))
    precio = db.Column(db.Numeric(scale=13, precision=2))
    fecha_nacimiento = db.Column(db.DateTime())
