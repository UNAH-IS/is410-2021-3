# Facturacion

## Problema
Hay PYMES que no cuentan con un sistema automatizado de facturacion.

## Solucion
- Registrar los productos y/o servicios de la PYME.
- Posibilidad de mostrar estos productos y servicios al usuario.
- Generar una orden de venta (trx)
- Establecer un formato impreso para el documento de factura.
- SAR?

## Diseno
### Clases (lluvia de idea)

- Empresa: nombre, telefono, direccion, vendedor
- Cliente: nombre, RTN, identidad
- Factura: codigo, instante (fecha+hora)
- Lista Productos: {Producto}, cantidad, precio unitario
- Montos: Descuento, ISV, Total, Subtotal
- Metodo de pago: Efectivo / Tarjeta
- Producto: codigo, nombre.
- Tipo de ISV: 0%, 15%, 18%


### Operaciones basicas del sistemas
Administrador | Vendedor |
---|---|
Crear productos/servicios | Crear una orden de venta |
Listar productos/servicios | Facturar |
Actualizar datos productos/servicios | Cobrar |
Dar de baja a productos/servicios | Editar orden de venta * |
Registrar usuarios | Dar de baja una orden de venta * |
Dar de baja a usuarios |
Listar una o varias ordenes de venta |
Exportar una o varias ordenes de venta en CSV |

(*) Solamente si la orden de venta no ha sido procesada.

### Clases definitivas

- Empresa:
    - nombre
    - telefono
    - direccion

- Usuario
    - codigo
    - nombre
    - password
    - ListarProductos()

- Vendedor(Usuario)
    - CrearVenta()

- Administrador(Usuario)
    - PermitirEditarVenta()
    - ExportarVentas()
    - CrearUsuario()
    - BajarUsuario()

- Impuesto
    - codigo
    - valor

- Producto
    - codigo
    - nombre
    - unidad
    - precio_unitario
    - tipo_producto
    - isv -> Impuesto
    - fecha_vencimiento

- Orden
    - codigo
    - fecha_emision
    - usuario
    - numero_caja
    - OrdenDetalle (list)
    - estado -> OrdenEstado {CREADO, PROCESADO, FACTURADO}
    - instante

- OrdenDetalle
    - producto
    - cantidad
    - descuento
    - subtotal = cantidad * producto.precio_unitario
    - impuesto = subtotal * (1 + Impuesto.valor/100)

- OrdenEstado
    - codigo
    - nombre

