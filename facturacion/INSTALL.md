# Instalacion de Paquetes Necesarios

## pip3
Es el instalador de paquetes para Python.

```bash
sudo apt install python3-pip
```

## Ambiente virtual (virtualenv)
Para instalarlo use el siguiente comando:
```
sudo apt install python3-virtualenv
```

Para crear un ambiente virtual denominado **env**. El cual es un directorio.
```
python3 -m virtualenv env
```

### Activacion
Para activar el ambiente virtual se dirige al directorio donde se creo. En este caso donde se creo la carpeta env.
```
source env/bin/activate
```
Esto permite que se active el entorno virual y es posible instalar paquetes para ese proyecto en particular.

### Desactivacion
Simplemente ejecute (en caso de estar activo) el comando:
```
deactivate
```

## SQLite
SQLite es una base de datos relacional.
```
sudo apt install sqlite3
```