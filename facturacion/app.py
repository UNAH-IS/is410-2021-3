from flask import Flask
from flask_sqlalchemy import SQLAlchemy

import config


app = Flask(__name__)

app.secret_key = config.SECRET_KEY
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = config.SQLALCHEMY_TRACK_MODIFICATIONS
app.config["SQLALCHEMY_DATABASE_URI"] = config.SQLALCHEMY_DATABASE_URI
db = SQLAlchemy(app)

# from models import Result


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/<name>")
def hello_name(name):
    return "Hello {}!".format(name)


if __name__ == "__main__":
    app.run()
