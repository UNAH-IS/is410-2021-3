import os


def get_dir(ruta_archivo: str):
    ruta_archivo = os.path.realpath(ruta_archivo)
    return os.path.dirname(ruta_archivo)


def get_dir_padre(ruta: str):
    ruta_hija = os.path.realpath(ruta)
    return os.path.dirname(ruta_hija)
