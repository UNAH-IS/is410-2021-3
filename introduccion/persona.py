class Persona:
    # atributo de clase (static)
    poblacion = 0

    def __init__(self, nombre):
        # el atributo (self.nombre) se inicializa con el argumento (nombre)
        self.nombre = nombre
        Persona.poblacion += 1

    def __str__(self) -> str:
        return self.nombre

    @classmethod
    def mostrar_poblacion(cls, msg):
        print(msg, cls.poblacion, "personas")

    @staticmethod
    def reiniciar_poblacion():
        Persona.poblacion = 0


if __name__ == "__main__":
    persona01 = Persona("Rocio Martinez")
    persona01.mostrar_poblacion("hola son ")

    lista_persona = list()
    # for (int i = 0; i < 10; i++)
    # range(10) == range(0, 10, 1)
    for i in range(10):
        nombre = f"clon {i}"
        lista_persona.append(Persona(nombre))

    # forech
    for persona in lista_persona:
        print(persona)

    Persona.reiniciar_poblacion()
    Persona.mostrar_poblacion("son:")
