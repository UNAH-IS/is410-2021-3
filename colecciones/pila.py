"""
Crear una clase denominada Pila que haga las tareas minimas de un Stack:
 - push
 - pop
 - clear
 - top
"""


class Pila:
    def __init__(self, nombre):
        self.nodos = []
        self.nombre = nombre

    def is_empty(self) -> bool:
        return self.nodos == []

    def push(self, dato):
        self.nodos.append(dato)

    def pop(self):
        if self.is_empty():
            return None

        return self.nodos.pop()
