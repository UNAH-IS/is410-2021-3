"""
list -> mutable (es posible modificar elementos internos)
tuple -> inmutable (solo de lectura)

En cuanto a lectura tuple es mas eficiente.

"""
import time
import sys

N = int(1e6)
lista = list()

for i in range(N):
    lista.append(i)

tupla = tuple(lista)

"""
Pruebas de memoria
"""

print(f"Memoria de lista: {sys.getsizeof(lista)} bytes")
print(f"Memoria de tupla: {sys.getsizeof(tupla)} bytes")


"""
Pruebas de tiempo
"""

inicio1 = time.time()
for elemento in lista:
    x = elemento
final1 = time.time()
print("Transcurrido en una lista:", final1 - inicio1)

time.sleep(10)

inicio2 = time.time()
for elemento in tupla:
    y = elemento
final2 = time.time()
print("Transcurrido en una tupla:", final2 - inicio2)
