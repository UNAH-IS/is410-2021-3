paises = {
    "HN": "Honduras",
    "US": "Estados Unidos",
    "MX": "Mexico",
    "GT": "Guatemala",
    "CR": {"nombre": "Costa Rica", "capital": "San Jose", "poblacion": 6000000},
}

alumno = {
    "cuenta": "20201001230",
    "asignaturas": [
        {
            "codigo": "IS-110",
            "nombre": "Introduccion a IS",
            "horario": [9, 10],
            "docente": "Jose Lopez",
            "carrera": "Ing. en Sistemas",
        },
        {
            "codigo": "MM-110",
            "nombre": "Matematicas I",
            "horario": [8, 9],
            "docente": "Ricardo Martinez",
        },
    ],
}


class Y:
    pass


y = Y()

x = {
    0: "Cero",
    str(False): "Falso",
    5.6: "Cinco punto seis",
    (1, 2, 3): "Tupla de 3 elementos",
    y: "El objeto de Y??",
}

try:
    print("x[1] = ", x[1])
except:
    print("No existe la clave")

"""
print("-----------")
print(x)
print("-----------")

asignaturas = alumno.get("cuentas", "00000000000")
print("Acceso mediante get:", asignaturas)

asignaturas = alumno["cuentas"]
print("Acceso mediante []: ", asignaturas)
"""

asignaturas = alumno.get("asignaturas")

for asignatura in asignaturas:
    carrera = asignatura.get("carrera", "N/D")
    docente = asignatura.get("docente", "Preguntar al Depto.")

    print(f"Docente: {docente}. Carrera: {carrera}")
