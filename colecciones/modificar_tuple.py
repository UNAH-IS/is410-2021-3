def modificar_tuple(t: tuple, posicion: int, valor) -> tuple:
    ls = list(t)
    ls[posicion] = valor
    t = tuple(ls)
    return t


tupla = (1, 2, 3, 4, 5)
resultado = modificar_tuple(tupla, 2, False)
print(resultado)
