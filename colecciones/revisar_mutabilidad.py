def revisar_mutabilidad_lista(ls: list):
    ls[0] = None


def revisar_mutabilidad_tupla(t: tuple):
    ls = list(t)
    revisar_mutabilidad_lista(ls)
    t = tuple(ls)
    print("Esto es dentro de la funcion: ", t)


lista = [10, 20, 30, 40]
tupla = (10, 20, 30, 40)
s = "12345"
revisar_mutabilidad_lista(lista)
print("Cambio la lista?", lista)

revisar_mutabilidad_tupla(tupla)
print("Cambio la tupla?", tupla)

revisar_mutabilidad_lista(s)
print("Cambio el str?", s)
