"""
# Forma1
import persona

p = persona.Persona()

#################################

# Forma 1 (alternativa)

import persona as per

p = per.Persona()
#################################

# Forma 2

from persona import Persona

p = Persona()
"""

"""
En Java esto es posible
Empleado emp;
Persona p;
emp = new Persona("Jose");

Si el Empleado tuviera un metodo denominado getSalario() y Persona no lo tiene
emp.getSalario() -> Esto seria un error.
"""

from persona import Persona
from trabajo import Empleado

p = Persona("Jose")
p.telefono = "9999-0000"
p.email = "jose@algo.com"
p.direccion = "Tegucigalpa, Honduras"
p.imprimir()

print("-" * 20)

emp = Empleado("Luis")
emp.telefono = "8888-1234"
emp.email = "luis@empresa.com"
emp.imprimir()

"""
print("Es instancia de Persona el objeto 'p'=", isinstance(p, Persona))
print("Es instancia de Persona el objeto 'emp'=", isinstance(emp, Persona))
print("Es instancia de Empleado el objeto 'p'=", isinstance(p, Empleado))
print("Es instancia de Empleado el objeto 'emp'=", isinstance(emp, Empleado))
"""
