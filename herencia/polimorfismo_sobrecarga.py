"""
def a(num1:int):
    pass

def a(num1:int, num2:int):
    pass

a(10)
"""


def sumar(*args):
    total = 0
    for x in args:
        total += x
    return total


print(sumar())
print(sumar(1))
print(sumar(1, 2))
print(sumar(1, 2, 3))
