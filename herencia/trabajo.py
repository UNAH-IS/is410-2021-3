from persona import Persona


class Empleado(Persona):
    def __init__(self, nombre, telefono):
        # Se llama al __init__ de la clase Base = Persona
        Persona.__init__(self, nombre, telefono)

        self.trabajo = None
        self.cargo = None
        self.telefono_trabajo = None
        self.direccion_trabajo = None

    def imprimir_datos_laborales(self):
        """
        Esta funcion es una nueva funcion que se encuentra unicamente en Empleado.
        """
        if self.trabajo:
            print("Trabajo: ", self.trabajo)

        if self.cargo:
            print("Cargo: ", self.cargo)

        if self.telefono_trabajo:
            print("Telefono: ", self.telefono_trabajo)

        if self.direccion_trabajo:
            print("Direccion: ", self.direccion_trabajo)

    def imprimir(self):
        super().imprimir()
        self.imprimir_datos_laborales()

    def __str__(self) -> str:
        """
        Sobreescritura del metodo de la clase base: Persona
        """
        return "Empleado: " + super().__str__()


class Cliente:
    def __init__(self, nombre, telefono):
        self.nombre = nombre
        self.telefono = telefono
        # super().__init__(nombre, telefono)

    # def imprimir(self):
    # super().imprimir()
    # print('Este es un cliente')
