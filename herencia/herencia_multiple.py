class Mitad:
    def __init__(self, valor) -> None:
        self.valor = valor

    def calcular(self):
        return self.valor / 2


class Doble:
    def __init__(self, valor) -> None:
        self.valor = valor

    def calcular(self):
        return self.valor * 2


class Calculadora(Doble, Mitad):
    def __init__(self, valor) -> None:
        super().__init__(valor)

    """def calcular(self):
        return Mitad.calcular(self)"""


obj = Calculadora(10)
print(obj.calcular())

# Revisar el orden de herencia
print(Calculadora.mro())
