"""
Prueba de polimorfismo utilizando objetos de tipo Persona y de tipo Empleado(Persona)
"""

from persona import Persona
from trabajo import Empleado, Cliente


persona = Persona("Alberto", "9999-0000")

empleado = Empleado("Julio", "9999-1234")
empleado.trabajo = "IHSS"
empleado.cargo = "Medico GRAL"
empleado.telefono_trabajo = "2222-1204"

cliente = Cliente("Maria", "8999-2334")

"""
# Forma 1
contactos = [persona, empleado]

# Forma 2
contactos = []
contactos.append(persona)
contactos.append(empleado)
"""
contactos = [persona, empleado, cliente]

for contacto in contactos:
    contacto.imprimir()
    print("=" * 20)
