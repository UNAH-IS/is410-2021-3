"""
Se desea probar los tipos de herencia.

La abstraccion sera a nivel de una aplicacion de una lista de contactos.
"""


class Persona:
    def __init__(self, nombre, telefono):
        # Recordar que self.nombre -> atributo y nombre -> es un parametro de __init__
        self.nombre = nombre
        self.telefono = telefono
        self.email = None
        self.direccion_casa = None

    def imprimir(self):
        print(self)
        if self.direccion_casa:
            print(f"Direccion (casa): {self.direccion_casa}")

        if self.email is not None:
            print(f"Email: {self.email}")

    def __str__(self) -> str:
        return f"{self.nombre} / {self.telefono}"
