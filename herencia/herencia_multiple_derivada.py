class A:
    def __init__(self) -> None:
        print("clase A")

    def test(self):
        print("Esto es una prueba... A")


class B:
    def __init__(self) -> None:
        print("clase B")


class C(B):
    def __init__(self) -> None:
        print("clase C")


class D(C, B, A):
    def __init__(self) -> None:
        print("clase D")

    def test(self):
        print("Esto es una prueba... D")


object_d = D()
object_d.test()
print(D.mro())
