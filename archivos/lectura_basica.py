"""
Por cada iteracion del bucle obtendremos una linea. Cada vez que se lee una linea
el objeto se comporta como cursor: lo que significa que solamente recorremos
una linea a la vez y esta no se repite hasta se abre de nuevo el archivo.
"""

file = open("test1.txt")

contador = 0
for x in file:
    print(x)
    if contador == 3:
        break
    contador += 1

print("Ocurre un segundo FOR")

for x in file:
    print(x)

print("Ocurre un tercer FOR")

for x in file:
    print(x)

file.close()
