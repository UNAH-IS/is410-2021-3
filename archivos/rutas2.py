import os


def get_dir(ruta_archivo: str):
    ruta_archivo = os.path.realpath(ruta_archivo)
    return os.path.dirname(ruta_archivo)


def get_dir_padre(ruta: str):
    ruta_hija = os.path.realpath(ruta)
    return os.path.dirname(ruta_hija)


def main():
    dir_archivo = get_dir(__file__)
    dir_raiz = get_dir_padre(dir_archivo)

    ruta_test1 = os.path.join(dir_raiz, "test1.txt")
    ruta_test2 = os.path.join(dir_raiz, "archivos", "test2.txt")

    file1 = open(ruta_test1)
    file2 = open(ruta_test2)

    print(file1)
    print(file2)


main()
