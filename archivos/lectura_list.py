"""
Obtenemos todo el archivo en un lista. Cada elemento es una linea.
"""

# Esta es una ruta relativa: Puede fallar segun la perspectiva. Se puede portar.
# file = open("test2.txt")

# Ruta absoluta: Evita los fallos por la perspectiva pero es dificil de portar el programa.
file = open("/home/unah/is410-2021-3/archivos/test2.txt")

datos = file.readlines()
print(type(datos))
print(datos)

file.close()
