import os

# pathlib -> modulo que se puede usar como reemplazo de path

# Windows -> C:\Users\unah\
# Unix -> /home/unah/

print("file:", __file__)
print("realpath:", os.path.realpath(__file__))
print("abspath:", os.path.abspath(__file__))
print("dirname:", os.path.dirname(os.path.realpath(__file__)))
