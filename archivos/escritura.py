"""
Persistencia
"""
import time
from os.path import dirname, abspath, join
import sys

# Establece la ruta
sys.path.append(dirname(dirname(abspath(__file__))))

from utils import dir

ruta_base = dir.get_dir(__file__)
ruta = join(ruta_base, "test3.txt")

file = open(ruta, "w")
paises = [
    "Honduras",
    "Nicaragua",
    "Guatemala",
    "El Salvador",
    "Costa Rica",
    "Mexico",
    "Estado Unidos",
    "Canada",
]

for pais in paises:
    file.write(f"{pais}\r\n")
    # time.sleep(1)

file.close()
